package storage;

import java.util.ArrayList;
import java.util.List;

import model.Laegemiddel;
import model.Patient;

public class Storage {
    private final List<Patient> patienter;
    private final List<Laegemiddel> laegemidler;

    public Storage() {
        patienter = new ArrayList<Patient>();
        laegemidler = new ArrayList<Laegemiddel>();
    }

    /**
     * Returnerer en liste med alle gemte patienter
     */
    public List<Patient> getAllPatienter() {
        return new ArrayList<Patient>(patienter);
    }

    /**
     * Gemmer patient
     */
    public void addPatient(Patient patient) {
        patienter.add(patient);
    }

    /**
     * Returnerer en liste med alle gemte lægemidler
     */
    public List<Laegemiddel> getAllLaegemidler() {
        return new ArrayList<Laegemiddel>(laegemidler);
    }

    /**
     * Gemmer lægemiddel
     */
    public void addLaegemiddel(Laegemiddel laegemiddel) {
        laegemidler.add(laegemiddel);
    }

}
