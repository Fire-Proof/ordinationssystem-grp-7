package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private Dosis[] doser = new Dosis[4];

	public DagligFast(Laegemiddel laegemiddel, LocalDate startDen, LocalDate slutDen) {
		super(laegemiddel, startDen, slutDen);
	}

	@Override
	public double doegnDosis() {
		double doegnDosis = 0;
		for (Dosis d : doser) {
			if (d != null) {
				doegnDosis += d.getAntal();
			}
		}
		return doegnDosis;
	}

	@Override
	public String getType() {
		return "Fast";
	}

	/**
	 * Opretter nye doser for morgen, middag, aften og nat
	 */
	public void createDoser(double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		if (morgenAntal > 0) {
			doser[0] = new Dosis(LocalTime.of(8, 0, 0, 0), morgenAntal);
		}
		if (middagAntal > 0) {
			doser[1] = new Dosis(LocalTime.of(12, 0, 0, 0), middagAntal);
		}
		if (aftenAntal > 0) {
			doser[2] = new Dosis(LocalTime.of(18, 0, 0, 0), aftenAntal);
		}
		if (natAntal > 0) {
			doser[3] = new Dosis(LocalTime.of(22, 0, 0, 0), natAntal);
		}
	}

	public Dosis[] getDoser() {
		return doser;
	}
}
