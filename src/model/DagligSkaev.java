package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DagligSkaev extends Ordination {
    private List<Dosis> doser = new ArrayList<>();

    public DagligSkaev(Laegemiddel laegemiddel, LocalDate startDen, LocalDate slutDen) {
        super(laegemiddel, startDen, slutDen);
    }

    public void opretDosis(LocalTime tid, double antal) {
        if (antal < 0) {
            throw new IllegalArgumentException("Antal enheder skal være positiv");
        }
        Dosis dosis = new Dosis(tid, antal);
        doser.add(dosis);
    }

    @Override
    public double doegnDosis() {
        double sum = 0;
        for (Dosis dosis : doser) {
            sum += dosis.getAntal();
        }
        return sum;
    }

    @Override
    public String getType() {
        return "Skæv";
    }

    public List<Dosis> getDoser() {
        return new ArrayList<>(doser);
    }
}
