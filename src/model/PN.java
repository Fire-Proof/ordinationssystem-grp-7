package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class PN extends Ordination {

    private List<LocalDate> doser = new ArrayList<>();
    private double antalEnheder;

    public PN(double antalEnheder, Laegemiddel laegemiddel, LocalDate startDen, LocalDate slutDen) {
        super(laegemiddel, startDen, slutDen);
        if (antalEnheder < 0) {
            throw new IllegalArgumentException("Antal enheder skal være positiv");
        }
        this.antalEnheder = antalEnheder;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen.
     * Returnerer true, hvis givesDen er inden for ordinationens gyldighedsperiode,
     * og datoen huskes.
     * Returnerer false ellers og datoen givesDen ignoreres.
     */
    public boolean givDosis(LocalDate givesDen) {
        if (givesDen.isBefore(startDen) || givesDen.isAfter(slutDen)) {
            return false;
        }

        doser.add(givesDen);

        return true;
    }

    // Skal have egen samletDosis()
    @Override
    public double samletDosis() {
        return getAntalGangeGivet() * getAntalEnheder();
    }

    @Override
    public double doegnDosis() {
        if (doser.size() == 0) {
            return 0;
        }

        doser.sort(null);

        LocalDate minDate = doser.get(0);
        LocalDate maxDate = doser.get(doser.size() - 1);

        return getAntalGangeGivet() * antalEnheder / (ChronoUnit.DAYS.between(minDate, maxDate) + 1);
    }

    @Override
    public String getType() {
        return "PN";
    }

    /**
     * Returnerer antal gange ordinationen er anvendt.
     */
    public int getAntalGangeGivet() {
        return doser.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

}
