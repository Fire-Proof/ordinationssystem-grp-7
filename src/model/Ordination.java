package model;

import java.time.*;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {
    protected final LocalDate startDen;
    protected final LocalDate slutDen;
    private Laegemiddel laegemidel;

	public Ordination(Laegemiddel laegemiddel, LocalDate startDen, LocalDate slutDen) {
		if (startDen.isAfter(slutDen) || startDen.equals(slutDen))
			throw new IllegalArgumentException("startDen skal være før slutDen"); 
		
        this.startDen = startDen;
        this.slutDen = slutDen;
        this.laegemidel = laegemiddel;
    }

	public Laegemiddel getLaegemiddel() {
		return laegemidel;
	}
	public void setLaegemiddel(Laegemiddel l) {
		assert laegemidel != l;
		
		laegemidel = l;
	}
	public void unsetLaegemiddel() {
		assert laegemidel != null;
		
		laegemidel = null;
	}
	
	public LocalDate getStartDen() {
        return startDen;
    }

    public LocalDate getSlutDen() {
        return slutDen;
    }

    /**
     * Returnerer antal hele dage mellem startdato og slutdato.
     * Begge dage inklusive.
     */
    public int antalDage() {
        return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
    }

    @Override
    public String toString() {
        return startDen.toString();
    }

    /**
     * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig.
     */
    public double samletDosis() {
        return antalDage() * doegnDosis();
    }

    /**
     * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen er gyldig.
     */
    public abstract double doegnDosis();

    /**
     * Returnerer ordinationstypen som en String
     */
    public abstract String getType();
}
