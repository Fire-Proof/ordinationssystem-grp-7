package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Laegemiddel;
import model.Patient;
import service.Service;

public class AnbefaletTest {
	Service service = new Service();
	Laegemiddel l1 = new Laegemiddel("navn", 1, 2, 3, "kilo");
	Laegemiddel l2 = new Laegemiddel("navn", 2, 4, 8, "kilo");

	@Test
	public void let() {
		Patient p = new Patient("999999-9999", "navn", 20);

		assertEquals(20, service.anbefaletDosisPrDoegn(p, l1), 0);
	}

	@Test
	public void letOevreGraense() {
		Patient p = new Patient("999999-9999", "navn", 24);

		assertEquals(24, service.anbefaletDosisPrDoegn(p, l1), 0);
	}
	
	@Test
	public void middelNedreGraense() {
		Patient p = new Patient("999999-9999", "navn", 25);

		assertEquals(50, service.anbefaletDosisPrDoegn(p, l1), 0);
	}
	
	@Test
	public void middel() {
		Patient p = new Patient("999999-9999", "navn", 90);

		assertEquals(180, service.anbefaletDosisPrDoegn(p, l1), 0);
	}
	
	@Test
	public void middelOevreGraense() {
		Patient p = new Patient("999999-9999", "navn", 120);

		assertEquals(240, service.anbefaletDosisPrDoegn(p, l1), 0);
	}
	
	@Test
	public void tungNedreGraense() {
		Patient p = new Patient("999999-9999", "navn", 121);

		assertEquals(363, service.anbefaletDosisPrDoegn(p, l1), 0);
	}

	@Test
	public void tung() {
		Patient p = new Patient("999999-9999", "navn", 130);

		assertEquals(390, service.anbefaletDosisPrDoegn(p, l1), 0);
	}
	
	@Test
	public void letHoejereEnhed() {
		Patient p = new Patient("999999-9999", "navn", 20);

		assertEquals(40, service.anbefaletDosisPrDoegn(p, l2), 0);
	}
	
	@Test
	public void middelHoejereEnhed() {
		Patient p = new Patient("999999-9999", "navn", 90);

		assertEquals(360, service.anbefaletDosisPrDoegn(p, l2), 0);
	}
	
	@Test
	public void tungHoejereEnhed() {
		Patient p = new Patient("999999-9999", "navn", 130);

		assertEquals(1040, service.anbefaletDosisPrDoegn(p, l2), 0);
	}
}
