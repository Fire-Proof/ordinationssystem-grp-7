package test;

import java.time.LocalDate;

import org.junit.Test;

import model.Laegemiddel;
import model.Ordination;

class OrdinationStub extends Ordination {

	public OrdinationStub(Laegemiddel laegemiddel, LocalDate startDen, LocalDate slutDen) {
		super(laegemiddel, startDen, slutDen);
	}

	@Override
	public double doegnDosis() {
		return 0;
	}

	@Override
	public String getType() {
		return null;
	}
}

public class OrdinationTest {

	@Test(expected = IllegalArgumentException.class)
	public void startDatoFoerSlutDato() {
		Laegemiddel l = new Laegemiddel("navn", 0, 0, 0, "enhed");

		new OrdinationStub(l, LocalDate.of(2017, 1, 2), LocalDate.of(2017, 1, 1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void sammeDato() {
		Laegemiddel l = new Laegemiddel("navn", 0, 0, 0, "enhed");

		new OrdinationStub(l, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 1));
	}
}
