package test;

import org.junit.Test;

import model.Patient;

public class PatientTest {

	@Test(expected = IllegalArgumentException.class)
	public void negativVaegt() {
		new Patient("999999-9999", "navn", -1);
	}
	
}
