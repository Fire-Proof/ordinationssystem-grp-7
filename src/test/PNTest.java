package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import model.Laegemiddel;
import model.PN;

public class PNTest {

	Laegemiddel laegemiddel = new Laegemiddel("laegemiddel", 1, 2, 2, "enhed");

	@Test
	public void nulEnheder() {
		PN pn = new PN(0, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));
		
		assertEquals(0, pn.doegnDosis(), 0);
		assertEquals(0, pn.samletDosis(), 0);
	}

	@Test
	public void enEnhed() {
		PN pn = new PN(1, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		assertEquals(0, pn.doegnDosis(), 0);
		assertEquals(0, pn.samletDosis(), 0);
	}

	@Test
	public void nulEnhederEnDosis() {
		PN pn = new PN(0, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 2));

		assertEquals(0, pn.doegnDosis(), 0);
		assertEquals(0, pn.samletDosis(), 0);
	}

	@Test
	public void enEnhedEnDosis() {
		PN pn = new PN(1, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 2));

		assertEquals(1, pn.doegnDosis(), 0);
		assertEquals(1, pn.samletDosis(), 0);
	}

	@Test
	public void toEnhederEnDosis() {
		PN pn = new PN(2, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 2));

		assertEquals(2, pn.doegnDosis(), 0);
		assertEquals(2, pn.samletDosis(), 0);
	}

	@Test
	public void enEnhedToDoserSammeDag() {
		PN pn = new PN(1, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 2));
		pn.givDosis(LocalDate.of(2017, 1, 2));

		assertEquals(2, pn.doegnDosis(), 0);
		assertEquals(2, pn.samletDosis(), 0);
	}

	@Test
	public void enEnhedToDoserToDage() {
		PN pn = new PN(1, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 2));
		pn.givDosis(LocalDate.of(2017, 1, 3));

		assertEquals(1, pn.doegnDosis(), 0);
		assertEquals(2, pn.samletDosis(), 0);
	}

	@Test
	public void toEnhederToDoserToDage() {
		PN pn = new PN(2, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 2));
		pn.givDosis(LocalDate.of(2017, 1, 3));

		assertEquals(2, pn.doegnDosis(), 0);
		assertEquals(4, pn.samletDosis(), 0);
	}

	@Test
	public void enEnhedToDageForkertRaekkefoelge() {
		PN pn = new PN(1, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 3));
		pn.givDosis(LocalDate.of(2017, 1, 2));

		assertEquals(1, pn.doegnDosis(), 0);
		assertEquals(2, pn.samletDosis(), 0);
	}

	@Test
	public void enEnhedToDageTidImellem() {
		PN pn = new PN(1, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));

		pn.givDosis(LocalDate.of(2017, 1, 1));
		pn.givDosis(LocalDate.of(2017, 1, 10));

		assertEquals(0.2, pn.doegnDosis(), 0);
		assertEquals(2, pn.samletDosis(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void negativEnheder() {
		new PN(-1, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 10));
	}

	@Test(expected = IllegalArgumentException.class)
	public void dosisGivetUdenForOrdinationsPeriode() {
		PN pn = new PN(0, laegemiddel, LocalDate.of(2017, 1, 1), LocalDate.of(2017, 1, 1));

		pn.givDosis(LocalDate.of(2017, 1, 2));
	}
}
