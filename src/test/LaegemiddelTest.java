package test;

import org.junit.Test;

import model.Laegemiddel;

public class LaegemiddelTest {

	@Test(expected = Exception.class)
	public void negativLet() {
		new Laegemiddel("navn", -1, 2, 3, "enhed");
	}

	@Test(expected = Exception.class)
	public void negativMiddel() {
		new Laegemiddel("navn", 1, -1, 3, "enhed");
	}

	@Test(expected = Exception.class)
	public void negativTung() {
		new Laegemiddel("navn", 1, 2, -1, "enhed");
	}
	
}
