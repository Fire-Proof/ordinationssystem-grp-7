package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import model.DagligSkaev;
import model.Laegemiddel;

public class DagligSkaevTest {
    private Laegemiddel laegemiddel;
    private LocalDate foerstJan;
    private LocalDate andenJan;
    private LocalDate tiendeJan;

    @Before
    public void setUp() throws Exception {
        laegemiddel = new Laegemiddel("Paracetamol", 1, 1.5, 2, "styk");
        foerstJan = LocalDate.of(2017, 1, 1);
        andenJan = LocalDate.of(2017, 1, 2);
        tiendeJan = LocalDate.of(2017, 1, 10);
    }

    @Test
    public void testDosisNul() {
        DagligSkaev skaev = new DagligSkaev(laegemiddel, foerstJan, andenJan);
        skaev.opretDosis(LocalTime.of(12, 0), 0);
        assertEquals(0, skaev.doegnDosis(), 0.001);
        assertEquals(0, skaev.samletDosis(), 0.001);
    }

    @Test
    public void testDosisÉnEnhed() {
        DagligSkaev skaev = new DagligSkaev(laegemiddel, foerstJan, andenJan);
        skaev.opretDosis(LocalTime.of(12, 0), 1);
        assertEquals(1, skaev.doegnDosis(), 0.001);
        assertEquals(2, skaev.samletDosis(), 0.001);
    }

    @Test
    public void testDosisToEnheder() {
        DagligSkaev skaev = new DagligSkaev(laegemiddel, foerstJan, andenJan);
        skaev.opretDosis(LocalTime.of(12, 0), 2);
        assertEquals(2, skaev.doegnDosis(), 0.001);
        assertEquals(4, skaev.samletDosis(), 0.001);
    }

    @Test
    public void testDosisToKlokkesletMedÉnEnhed() {
        DagligSkaev skaev = new DagligSkaev(laegemiddel, foerstJan, andenJan);
        skaev.opretDosis(LocalTime.of(12, 0), 1);
        skaev.opretDosis(LocalTime.of(13, 0), 1);
        assertEquals(2, skaev.doegnDosis(), 0.001);
        assertEquals(4, skaev.samletDosis(), 0.001);
    }

    @Test
    public void testDosisÉnMedForskudtDato() {
        DagligSkaev skaev = new DagligSkaev(laegemiddel, foerstJan, tiendeJan);
        skaev.opretDosis(LocalTime.of(12, 0), 1);
        assertEquals(1, skaev.doegnDosis(), 0.001);
        assertEquals(10, skaev.samletDosis(), 0.001);
    }

    @Test
    public void testDosisToKlokkesletMedForskudtDato() {
        DagligSkaev skaev = new DagligSkaev(laegemiddel, foerstJan, tiendeJan);
        skaev.opretDosis(LocalTime.of(12, 0), 1);
        skaev.opretDosis(LocalTime.of(13, 0), 1);
        assertEquals(2, skaev.doegnDosis(), 0.001);
        assertEquals(20, skaev.samletDosis(), 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDosisNegativVaerdi() {
        DagligSkaev skaev = new DagligSkaev(laegemiddel, foerstJan, andenJan);
        skaev.opretDosis(LocalTime.of(12, 0), -11);
    }
}
