package test;

import model.Laegemiddel;
import model.Patient;
import org.junit.Before;
import org.junit.Test;
import service.Service;
import storage.Storage;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class StatistikTest {
    private Service service;
    private Patient p50kg;
    private Patient p65kg;
    private Patient p100kg;
    private Patient p101kg;
    private Laegemiddel lmX;
    private Laegemiddel lmY;
    private LocalDate start;
    private LocalDate slut;

    @Before
    public void setUp() throws Exception {
        service = new Service(new Storage());
        p50kg = service.opretPatient("999999-9999", "Test Patient 50kg", 50);
        p65kg = service.opretPatient("999999-9999", "Test Patient 65kg", 65);
        p100kg = service.opretPatient("999999-9999", "Test Patient 100kg", 100);
        p101kg = service.opretPatient("999999-9999", "Test Patient 101kg", 101);
        lmX = service.opretLaegemiddel("x", 1, 2, 3, "stk");
        lmY = service.opretLaegemiddel("y", 1, 2, 3, "stk");
        start = LocalDate.of(2017, 1, 1);
        slut = LocalDate.of(2017, 1, 2);

    }

    @Test
    public void testStatistikIngenOrd() {
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 10, lmX);
        assertEquals(0, result);
    }

    @Test
    public void testStatistikUnderOrd() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 10, lmX);
        assertEquals(0, result);
    }

    @Test
    public void testStatistikOverOrd() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(90, 100, lmX);
        assertEquals(0, result);
    }

    @Test
    public void testStatistikFindEnPN() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 100, lmX);
        assertEquals(1, result);
    }

    @Test
    public void testStatistikFindEnFast() {
        service.opretDagligFastOrdination(start, slut, p50kg, lmX, 1, 1, 1, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 100, lmX);
        assertEquals(1, result);
    }

    @Test
    public void testStatistikFindEnSkaev() {
        service.opretDagligSkaevOrdination(start, slut, p50kg, lmX, new LocalTime[] {},
            new double[] {});
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 100, lmX);
        assertEquals(1, result);
    }

    @Test
    public void testStatistikFind2() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.opretPNOrdination(start, slut, p65kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 100, lmX);
        assertEquals(2, result);
    }

    @Test
    public void testStatistikFind2NulTilTres() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.opretPNOrdination(start, slut, p65kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 60, lmX);
        assertEquals(1, result);
    }

    @Test
    public void testStatistikFindForkertLaegemiddel() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.opretPNOrdination(start, slut, p65kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 100, lmY);
        assertEquals(0, result);
    }

    @Test
    public void testStatistikFindYBlandForskellige() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.opretPNOrdination(start, slut, p65kg, lmY, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(0, 100, lmX);
        assertEquals(1, result);
    }

    @Test
    public void testStatistikFindEdgeCase50til100() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.opretPNOrdination(start, slut, p100kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(50, 100, lmX);
        assertEquals(2, result);
    }

    @Test
    public void testStatistikFindEdgeCase50til100og101() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.opretPNOrdination(start, slut, p100kg, lmX, 1);
        service.opretPNOrdination(start, slut, p101kg, lmX, 1);
        int result = service.antalOrdinationerPrVægtPrLægemiddel(50, 100, lmX);
        assertEquals(2, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStatistikStartvaegtStorreEndSlut() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.antalOrdinationerPrVægtPrLægemiddel(100, 0, lmX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStatistikNegativStartVaerdi() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.antalOrdinationerPrVægtPrLægemiddel(-1, 100, lmX);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStatistikIngenLaegemiddel() {
        service.opretPNOrdination(start, slut, p50kg, lmX, 1);
        service.antalOrdinationerPrVægtPrLægemiddel(0, 100, null);
    }

}