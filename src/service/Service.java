package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import model.DagligFast;
import model.DagligSkaev;
import model.Laegemiddel;
import model.Ordination;
import model.PN;
import model.Patient;
import storage.Storage;

public class Service {
    private static Storage storage;

    public Service(Storage storage) {
        Service.storage = storage;
    }

    public Service() {
    }

    /**
     * Opretter og returnerer en PN ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException,
     * og ordinationen oprettes ikke.
     */
    public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen,
        Patient patient, Laegemiddel laegemiddel, double antal) {
        PN pn = new PN(antal, laegemiddel, startDen, slutDen);

        patient.addOrdination(pn);

        return pn;
    }

    /**
     * Opretter og returnerer en DagligFast ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException,
     * og ordinationen oprettes ikke.
     */
    public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen,
        Patient patient, Laegemiddel laegemiddel,
        double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
        DagligFast df = new DagligFast(laegemiddel, startDen, slutDen);
        df.createDoser(morgenAntal, middagAntal, aftenAntal, natAntal);

        patient.addOrdination(df);

        return df;

    }

    /**
     * Opretter og returnerer en DagligSkæv ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException,
     * og ordinationen oprettes ikke.
     * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige
     * kastes en IllegalArgumentException.
     */
    public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen,
        Patient patient, Laegemiddel laegemiddel,
        LocalTime[] klokkeSlet, double[] antalEnheder) {

        if (klokkeSlet.length != antalEnheder.length) {
            throw new IllegalArgumentException(
                "der skal være den samme antal klokkeslet som antal enheder");
        }

        DagligSkaev dagligSkaev = new DagligSkaev(laegemiddel, startDen, slutDen);
        patient.addOrdination(dagligSkaev);

        for (int i = 0; i < klokkeSlet.length; i++) {
            dagligSkaev.opretDosis(klokkeSlet[i], antalEnheder[i]);
        }

        return dagligSkaev;
    }

    /**
     * En dato for hvornår ordinationen anvendes tilføjes ordinationen.
     * Hvis datoen ikke er indenfor ordinationens gyldighedsperiode
     * kastes en IllegalArgumentException.
     */
    public void ordinationPNAnvendt(PN ordination, LocalDate dato) {
    	boolean dosisGivet = ordination.givDosis(dato);
    	
        if (!dosisGivet) throw new IllegalArgumentException("datoen skal være mellem start og slut dato");
    }

    /**
     * Den anbefalede dosis for den pågældende patient (der skal tages hensyn
     * til patientens vægt). Det er en forskellig enheds faktor der skal
     * anvendes, og den er afhængig af patientens vægt.
     */
    public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
        double result;
        if (patient.getVaegt() < 25) {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
        }
        else if (patient.getVaegt() > 120) {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
        }
        else {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
        }
        return result;
    }

    /**
     * For et givent vægtinterval og et givent lægemiddel
     * hentes antallet af ordinationer.
     */
    public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut,
        Laegemiddel laegemiddel) {
        // Disse exceptions bliver ikke fanget af GUI
        // Derfor vil der opstå fejl
        if (vægtStart > vægtSlut){
            throw new IllegalArgumentException("Start vægt skal være mindre end Slut");
        } if (vægtStart < 0){
            throw new IllegalArgumentException("Start vægt positiv");
        } if (laegemiddel == null){
            throw new IllegalArgumentException("Lægemiddel må ikke være null");
        }

        int antal = 0;

        for (Patient p : storage.getAllPatienter()) {
            if (p.getVaegt() >= vægtStart && p.getVaegt() <= vægtSlut) {
                for (Ordination o : p.getOrdinationer()) {
                    if (o.getLaegemiddel() == laegemiddel) {
                        antal += 1;
                    }
                }
            }
        }

        return antal;
    }

    public List<Patient> getAllPatienter() {
        return storage.getAllPatienter();
    }

    public List<Laegemiddel> getAllLaegemidler() {
        return storage.getAllLaegemidler();
    }

    public Patient opretPatient(String cpr, String navn, double vaegt) {
        Patient p = new Patient(cpr, navn, vaegt);
        storage.addPatient(p);
        return p;
    }

    public Laegemiddel opretLaegemiddel(String navn,
        double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
        double enhedPrKgPrDoegnTung, String enhed) {
        Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet,
            enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung, enhed);
        storage.addLaegemiddel(lm);
        return lm;
    }

    public void createSomeObjects() {
        Patient jj = opretPatient("121256-0512", "Jane Jensen", 63.4);
        Patient fm = opretPatient("070985-1153", "Finn Madsen", 83.2);
        opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        Patient un = opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        opretPatient("090149-2529", "Ib Hansen", 87.7);

        Laegemiddel ace = opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        Laegemiddel par = opretLaegemiddel("Paracetamol", 1, 1.5, 2, "mL");
        Laegemiddel fuc = opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12),
            jj, par, 123);

        opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14),
            jj, ace, 3);

        opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25),
            un, fuc, 5);

        opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12),
            jj, par, 123);

        opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12),
            fm, par, 2, -1, 1, -1);

        LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
            LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };

        opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24),
            fm, fuc, kl, an);
    }

}
